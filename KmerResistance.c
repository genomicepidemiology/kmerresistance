/* Philip T.L.C. Clausen Jan 2017 s123580@student.dtu.dk */

/*
 Copyright (c) 2017, Philip Clausen, Technical University of Denmark
 All rights reserved.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
#define _XOPEN_SOURCE 600
#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "pherror.h"
#include "qseqs.h"
#define VERSION "2.2.0"

int strpos_last(const char* str1, const char* str2) {
	
	char *strp;
	int i, len1, len2;
	
	len1 = strlen(str1);
	len2 = strlen(str2);
	if(len1 == 0 || len2 == 0 || len1 - len2 < 0) {
		return -1;
	}
	
	strp = (char*)(str1 + len1 - len2 + 1);
	for(i = len1 - len2; i >= 0; i--) {
		if(*--strp == *str2 && strncmp(strp,str2,len2) == 0) {
			return i;
		}
	}
	return -1;
}

int fget2(Qseqs *line, FILE *file, char stopChar) {
	
	int c, len, size;
	unsigned char *ptr;
	
	len = 0;
	size = line->size;
	ptr = line->seq;
	while((c = fgetc(file)) != EOF) {
		if(++len == size) {
			if(!(line->seq = realloc(line->seq, (size <<= 1)))) {
				ERROR();
			} else {
				ptr = line->seq + len;
				line->size = size;
			}
		}
		if((*ptr++ = c) == stopChar) {
			*--ptr = 0;
			line->len = --len;
			return 1;
		}
	}
	*ptr = 0;
	line->len = len;
	
	return 0;
}

char * stripSpace(const unsigned char *src) {
	
	char *ptr;
	
	ptr = (char *) src;
	while(*ptr != 0 && isspace(*ptr)) {
		++ptr;
	}
	
	return ptr;
}

#define skipLine(file, c, stopChar) while((c = fgetc(file)) != EOF && c != stopChar)

void helpMessage(int exeStatus) {
	FILE *helpOut;
	if(exeStatus == 0) {
		helpOut = stdout;
	} else {
		helpOut = stderr;
	}
	fprintf(helpOut, "# KmerResistance-2.0 correlates genes to species from WGS data, by mapping reads with KMA\n");
	fprintf(helpOut, "# Options are:\t\tDesc:\t\t\t\tDefault:\t\tRequirements:\n");
	fprintf(helpOut, "#\n");
	fprintf(helpOut, "#\t-i\t\tInput/query file name\t\t\t\t\tREQUIRED\n");
	fprintf(helpOut, "#\t-o\t\tOutput file\t\t\t\t\t\tREQUIRED\n");
	fprintf(helpOut, "#\t-t_db\t\tTemplate DB\t\t\t\t\t\tREQUIRED\n");
	fprintf(helpOut, "#\t-s_db\t\tSpecies DB\t\t\t\t\t\tREQUIRED\n");
	fprintf(helpOut, "#\t-id\t\tID threshhold\t\t\t70.0\n");
	fprintf(helpOut, "#\t-hid\t\tHard ID threshhold\t\t\t1.0\n");
	fprintf(helpOut, "#\t-dct\t\tDepth correction threshhold\t0.1\n");
	fprintf(helpOut, "#\t-kma\t\talternative KMA\t\tkma\n");
	fprintf(helpOut, "#\t-v\t\tVersion\n");
	fprintf(helpOut, "#\t-h\t\tShows this help message\n");
	fprintf(helpOut, "#\n");
	exit(exeStatus);
}

int main(int argc, char *argv[]) {
	
	int args, KMA_call_len, out_len, stop;
	char *outputfilename, *t_db, *s_db, *KMA_dest, *KMA_call, *exeBasic;
	double depth_corr_t, ID_t, HID_t, ID, depth, depth_corr, expDepth;
	FILE *output, *input;
	Qseqs *line, *template_name, *score, *expected, *t_len, *q, *p, *t_cov;
	Qseqs *q_cov, *q_id;
	
	/* set defaults */
	depth_corr_t = 0.1;
	ID_t = 70.0;
	HID_t = 1.0;
	outputfilename = 0;
	t_db = 0;
	s_db = 0;
	KMA_dest = smalloc(4);
	strcpy(KMA_dest, "kma");
	KMA_call_len = 11;
	
	/* parse cmd-line */
	args = 1;
	while(args < argc) {
		if(strcmp(argv[args], "-o") == 0) {
			args++;
			if(args < argc) {
				KMA_call_len += (strlen(argv[args]) + 4);
				out_len = strlen(argv[args]);
				outputfilename = (char*) calloc(out_len + 16, sizeof(char));
				if(!outputfilename) {
					ERROR();
				}
				strcpy(outputfilename, argv[args]);
				strcat(outputfilename, ".KmerRes");
			}
		} else if(strcmp(argv[args], "-t_db") == 0) {
			args++;
			if(args < argc) {
				t_db = (char*) calloc(strlen(argv[args]) + 1, sizeof(char));
				if(!t_db) {
					ERROR();
				}
				strcpy(t_db, argv[args]);
				KMA_call_len += (strlen(argv[args]) + 1);
			}
		} else if(strcmp(argv[args], "-s_db") == 0) {
			args++;
			if(args < argc) {
				s_db = (char*) calloc(strlen(argv[args]) + 1, sizeof(char));
				if(!s_db) {
					ERROR();
				}
				strcpy(s_db, argv[args]);
				KMA_call_len += (strlen(argv[args]) + 1);
			}
		} else if(strcmp(argv[args], "-id") == 0) {
			args++;
			if(args < argc) {
				ID_t = strtod(argv[args], &exeBasic);
				if(*exeBasic != 0) {
					fprintf(stderr, "%s\n", exeBasic);
					exit(1);
				} else if(ID_t < 0) {
					fprintf(stderr, "Invalid threshold\n");
					exit(1);
				}
			}
		}  else if(strcmp(argv[args], "-hid") == 0) {
			args++;
			if(args < argc) {
				HID_t = strtod(argv[args], &exeBasic);
				if(*exeBasic != 0) {
					fprintf(stderr, "%s\n", exeBasic);
					exit(1);
				} else if(HID_t < 0) {
					fprintf(stderr, "Invalid threshold\n");
					exit(1);
				}
			}
		} else if(strcmp(argv[args], "-dct") == 0) {
			args++;
			if(args < argc) {
				depth_corr_t = strtod(argv[args], &exeBasic);
				if(*exeBasic != 0) {
					fprintf(stderr, "%s\n", exeBasic);
					exit(1);
				} else if(depth_corr_t < 0) {
					fprintf(stderr, "Invalid threshold\n");
					exit(1);
				}
			}
		} else if(strcmp(argv[args], "-kma") == 0) {
			args++;
			if(args < argc) {
				free(KMA_dest);
				KMA_dest = strdup(argv[args]);
				if(!KMA_dest) {
					fprintf(stderr, "Error: %d (%s)\n", errno, strerror(errno));
					exit(errno);
				}
				KMA_call_len += strlen(argv[args]);
			}
		} else if(strcmp(argv[args], "-v") == 0) {
			fprintf(stderr, "KmerResistance-%s\n", VERSION);
			exit(0);
		} else if(strcmp(argv[args], "-h") == 0) {
			helpMessage(0);
		} else {
			KMA_call_len += strlen(argv[args]) + 1;
			if(strcmp(argv[args], "-i") == 0) {
				stop = 0;
				args++;
				while(stop == 0 && args < argc) {
					if(*(argv[args]) != '-') {
						KMA_call_len += strlen(argv[args]) + 1;
						args++;
					} else {
						stop = 1;
					}
				}
				args--;
			}
		}
		args++;
	}
	if(outputfilename == 0 || t_db == 0 || s_db == 0) {
		fprintf(stderr, "# Too few arguments handed\n");
		fprintf(stderr, "# Printing help message:\n");
		helpMessage(1);
	}
	
	/* prepare KMA alignment */
	KMA_call_len += 8;
	KMA_call = smalloc(KMA_call_len + strpos_last(*argv, "/") + 4);
	
	/* add options to KMA */
	KMA_call_len = sprintf(KMA_call, "%s ", KMA_dest);
	args = 1;
	while(args < argc) {
		if(strcmp(argv[args], "-t_db") == 0) {
			args++;
		} else if(strcmp(argv[args], "-s_db") == 0) {
			args++;
		} else if(strcmp(argv[args], "-id") == 0) {
			args++;
		} else if(strcmp(argv[args], "-dct") == 0) {
			args++;
		} else if(strcmp(argv[args], "-kma") == 0) {
			args++;
		} else if(strcmp(argv[args], "-Sparse") == 0) {
			args++;
		} else if(strcmp(argv[args], "-h") != 0) {
			KMA_call_len += sprintf(KMA_call + KMA_call_len, "%s ", argv[args]);
		}
		args++;
	}
	KMA_call_len += sprintf(KMA_call + KMA_call_len, "-t_db ");
	
	/* map genes */
	fprintf(stderr, "# Aligning gene data.\n");
	sprintf(KMA_call, "%s%s", KMA_call, t_db);
	if(system(KMA_call) != 0) {
		fprintf(stderr, "# Gene alignment failed\n");
		exit(1);
	}
	KMA_call[KMA_call_len] = 0;
	
	/* map species */
	fprintf(stderr, "# Finding species\n");
	sprintf(KMA_call, "%s%s -Sparse", KMA_call, s_db);
	if(system(KMA_call) != 0) {
		fprintf(stderr, "# Species mapping failed\n");
		exit(1);
	}
	KMA_call[KMA_call_len] = 0;
	
	/* correlate species */
	fprintf(stderr, "# Mapping done\n");
	fprintf(stderr, "# Correlating results\n");
	line = setQseqs(256);
	template_name = setQseqs(256);
	score = setQseqs(32);
	expected = setQseqs(32);
	t_len = setQseqs(32);
	q = setQseqs(32);
	p = setQseqs(32);
	t_cov = setQseqs(32);
	q_id = setQseqs(32);
	q_cov = setQseqs(32);
	
	output = sfopen(outputfilename, "wb");
	fprintf(output, "#Template\tScore\tExpected\ttemplate length\tq_value\tp_value\ttemplate_id\ttemplate_coverage\tquery_id\tquery_coverage\tdepth\tdepth_corr\n");
	strcpy((outputfilename + out_len), ".spa");
	input = sfopen(outputfilename, "rb");
	
	/* retrieve species information */
	skipLine(input, args, '\n');
	fget2(template_name, input, '\t');
	if(template_name->len) {
		skipLine(input, args, '\t');
		fget2(score, input, '\t');
		fget2(expected, input, '\t');
		fget2(t_len, input, '\t');
		fget2(q_cov, input, '\t');
		fget2(t_cov, input, '\t');
		ID = strtod(stripSpace(t_cov->seq), &exeBasic);
		if(*exeBasic != 0) {
			fprintf(stderr, "%s\n", exeBasic);
			exit(1);
		}
		
		fget2(line, input, '\t');
		expDepth = strtod(stripSpace(line->seq), &exeBasic);
		if(*exeBasic != 0) {
			fprintf(stderr, "%s\n", exeBasic);
			exit(1);
		}
		skipLine(input, args, '\t');
		skipLine(input, args, '\t');
		skipLine(input, args, '\t');
		fget2(q, input, '\t');
		fget2(p, input, '\n');
		ID_t *= (1 - pow(2.718281828, (-0.5) * expDepth));
		ID_t = ID_t < HID_t ? HID_t : ID_t;
		fprintf(output, "%s\t%s\t%s\t%s\t%s\t%s\t%3.2f\t%s\t%s\t%s\t%5.2f\t%1.4f\n", template_name->seq, score->seq, expected->seq, t_len->seq, q->seq, p->seq, ID, t_cov->seq, q_cov->seq, q_cov->seq, expDepth, (1 - pow(2.718281828, -1)));
	} else {
		fprintf(stderr, "Could not determine host.\n");
		expDepth = 1.0e-6;
		ID_t = HID_t;
	}
	fclose(input);
	
	fprintf(stderr, "# Adjusted ID threshold: %.2f\n", ID_t);
	
	/* retrieve template information */
	strcpy((outputfilename + out_len), ".res");
	input = sfopen(outputfilename, "rb");
	skipLine(input, args, '\n');
	while(!feof(input)) {
		fget2(template_name, input, '\t');
		fget2(score, input, '\t');
		fget2(expected, input, '\t');
		fget2(t_len, input, '\t');
		fget2(line, input, '\t');
		ID = strtod(stripSpace(line->seq), &exeBasic);
		if(*exeBasic != 0) {
			fprintf(stderr, "%s\n", exeBasic);
			exit(1);
		}
		fget2(t_cov, input, '\t');
		fget2(q_id, input, '\t');
		fget2(q_cov, input, '\t');
		fget2(line, input, '\t');
		depth = strtod(stripSpace(line->seq), &exeBasic);
		if(*exeBasic != 0) {
			fprintf(stderr, "%s\n", exeBasic);
			exit(1);
		}
		fget2(q, input, '\t');
		fget2(p, input, '\n');
		depth_corr = (1 - pow(2.718281828, (-1) * depth / expDepth));
		if(depth_corr_t <= depth_corr && ID_t <= ID) {
			fprintf(output, "%s\t%s\t%s\t%s\t%s\t%s\t%3.2f\t%s\t%s\t%s\t%5.2f\t%1.4f\n", template_name->seq, score->seq, expected->seq, t_len->seq, q->seq, p->seq, ID, t_cov->seq, q_id->seq, q_cov->seq, depth, depth_corr);
		}
	}
	fclose(input);
	fclose(output);
	fprintf(stderr, "# DONE\n");
	
	return 0;
}

